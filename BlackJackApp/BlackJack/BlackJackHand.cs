﻿using BlackJackApp.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackJackApp.BlackJack
{
    public class BlackJackHand : Hand
    {
        public int Score()
        {
            return 0;
        }
        public bool Busted()
        {
            return false;
        }
        public bool Is21()
        {
            return false;
        }
        public bool IsBlackjack()
        {
            return false;
        }
        public BlackJackHand(List<Card> _cards) : base(_cards)
        {

        }
    }
}
