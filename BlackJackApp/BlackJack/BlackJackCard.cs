﻿using BlackJackApp.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackJackApp.BlackJack
{
    public class BlackJackCard : Card
    {
        public int ValueCard()
        {
            return 0;
        }
        public int MinValue()
        {
            return 0;
        }
        public int MaxValue()
        {
            return 0;
        }
        public bool IsAce()
        {
            return false;
        }
        public bool IsFace()
        {
            return false;
        }

        public BlackJackCard(int _value, CardSuit _suit) : base(_value, _suit)
        {

        }
    }
}
