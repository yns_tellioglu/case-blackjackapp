﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackJackApp.Domain
{
    public class Card
    {
        public Card(int _value, CardSuit _suit)
        {
            value = _value;
            suit = _suit;
        }
        public int value { get; set; }
        public CardSuit suit { get; set; } = new CardSuit();
    }

}
