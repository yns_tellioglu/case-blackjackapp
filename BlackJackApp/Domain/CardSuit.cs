﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackJackApp.Domain
{
    public enum CardSuit
    {
        Club,
        Diamond,
        Heart,
        Spade
    }
}
