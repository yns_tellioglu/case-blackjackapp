﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackJackApp.Domain
{
    public class Deck
    {
        public int numOfCards { get; set; }
        public List<Card> cards { get; set; } = new List<Card>();

        public Deck(int _numOfCards, List<Card> _cards)
        {
            numOfCards = _numOfCards;
            cards = _cards;
        }
    }
}
